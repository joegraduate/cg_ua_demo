<?php
abstract class UaDemoPlaceBlocksMigration extends UaDemoBeanMigration {
  /**
   * Runs before an import starts.
   *
   * Used to disable any rules which could cause problems during
   * the import.
   */

    //  public function preImport() {
    //    parent::preImport();
    //  }

  /**
   * Ran after completion of a migration.
   *
   * Used to turn any rules that were disabled back on.
   */
  public function postImport() {
    parent::postImport();

    if (!empty($this->arguments['place_in_footer']) && theme_exists('ua_zen')) {
      db_update('block')
        ->fields(array('region' => 'footer'))
        ->condition('delta', $this->arguments['place_in_footer'], 'IN')
        ->condition('theme', 'ua_zen')
        ->execute();
    }
  }
}
?>