<?php

/**
 * @file
 * Add content to demonstrate a UA Card block.
 */

/**
 * Makes demonstration UA Card bean content from pre-defined data.
 *
 * The UA Block Types feature for fieldable blocks based on the Bean module
 * provides the entity used for this.
 *
 * The field contents come from a JSON file.
 */
class UaDemoCardMigration extends UaDemoBeanMigration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments, 'ua_card',
      t('Make demonstration UA Card content from pre-defined data.'));

    // Documented lists of source data fields.
    // See ua_block_types.features.field_instance.inc.
    $data_fields = array(
      'label' => t('Descriptive title that the administrative interface uses'),
      'ua_blurb_text' => t('Blurb text'),
      'call_to_action' => t('Call to action link'),
    );

    // Image fields...
    $image_src_field = 'slide_image';
    $image_fields = array(
      $image_src_field => t('Slide image'),
    );

    // Titles for links...
    $link_title_fields = array(
      'call_to_action_title' => t('Call to action title'),
    );

    // All the fields to migrate.
    $fields = $this->getSourceKeyField() + $data_fields + $link_title_fields + $image_fields;

    // Source definition.
    $this->source = new MigrateSourceJSON($this->jsonFilePath(), $this->getSourceKeyFieldName(), $fields);

    // JSON names to fields mappings.
    $this->addSimpleMappings(array('label'));

    // The title has no prefix.
    $this->addSimpleMappings(array('title'));

    $this->addFieldMapping('field_call_to_action', 'call_to_action');

    $this->addFieldMapping('field_ua_blurb_text', 'ua_blurb_text');

    $this->addFieldMapping('field_call_to_action:title', 'call_to_action_title');

    // Images.
    $image_dst_field = 'field_' . $image_src_field;
    $this->addFieldMapping($image_dst_field, $image_src_field);
    $this->addFieldMapping($image_dst_field . ':file_replace')
         ->defaultValue(FILE_EXISTS_REPLACE);
    $this->addFieldMapping($image_dst_field . ':file_class')
         ->defaultValue('MigrateFileUri');
    $this->addFieldMapping($image_dst_field . ':source_dir')
         ->defaultValue($this->imagePath());

  }

}
